<?php

if (!isset($_GET['s'])) {
    exit();
}

$log = json_decode(trim(file_get_contents($_GET['s'] . '.json')));

$log_index = (int) $_GET['index'];

?>
<?php if (!empty($log)) : ?>
<?php if ($log_index) : ?>
    <?php for ($i = $log_index; $i < count($log); $i++) : ?>
    <li data-source="<?= $log[$i]->source; ?>" data-target="<?= $log[$i]->target; ?>" class="list-group-item"><code><?= $log[$i]->time; ?></code><strong><?= $log[$i]->user; ?></strong><em><?= $log[$i]->text; ?></em></li>
    <?php endfor; ?>
<?php else : ?>
    <?php foreach ($log as $index => $line) : ?>
    <li data-source="<?= $line->source; ?>" data-target="<?= $line->target; ?>" class="list-group-item"><code><?= $line->time; ?></code><strong><?= $line->user; ?></strong><em><?= $line->text; ?></em></li>
    <?php endforeach; ?>
<?php endif; ?>
<?php endif; ?>