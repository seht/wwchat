<?php 

if (!isset($_GET['s']) || !$_GET['s']) {
    header('Location: ?s=' . uniqid());
    exit();
}

$session = $_GET['s'];

session_start();

define('KEY', '');

$args = ['key' => KEY, 'ui' => 'en'];
$query = '?' . http_build_query($args);

$lang = json_decode(file_get_contents('https://translate.yandex.net/api/v1.5/tr.json/getLangs' . $query));

$guest = 'guest-' . time();

?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
    
    <style>
    .form-actions {margin-top: 15px;}
    .form-container {padding: 0;}
    .form-controls button {width: 100%;}
    </style>

    <script src="js/mespeak/mespeak.js"></script>
    
    <script>
    
    </script>

</head>

<body>

<div class="container-fluid form-container">

    <form id="chat-form" class="form" method="post" target="session.php?s=<?= $session; ?>">
    
        <nav class="navbar navbar-default">
          <div class="container-fluid">
          <!-- 
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
            </div>
             -->
            <div id="navbar" class="navbar-collapse">
              <div class="navbar-right">
                <div class="btn-group" role="group" aria-label="...">
                   <button type="button" class="btn btn-default navbar-btn" data-action="edit-user" disabled><?= (isset($_SESSION[$session]['user'])) ? $_SESSION[$session]['user'] : $guest; ?></button>
                   <button type="button" class="btn btn-default navbar-btn" data-action="toggle-audio"><span class="glyphicon glyphicon glyphicon-volume-up"></span></button>
                   <button type="button" class="btn btn-default navbar-btn" data-action="share"><span class="glyphicon glyphicon glyphicon-share-alt"></span></button>
                   <button type="button" class="btn btn-default navbar-btn" data-action="terminate"><span class="glyphicon glyphicon glyphicon-trash"></span></button>
                   <a class="btn btn-default navbar-btn" data-action="new-session" href="?s=0" target="_blank"><span class="glyphicon glyphicon-new-window"></span></a>
                </div>
                <input style="display: none;" type="hidden" name="user" placeholder="Enter a nickname" required disabled value="<?= (isset($_SESSION[$session]['user'])) ? $_SESSION[$session]['user'] : $guest; ?>">
              </div>
            </div>
          </div>
        </nav>

        <div class="chat-output">
            <ul class="list-group">
        	<?php include 'log.php'; ?>
        	</ul>
        </div>

    	<div class="row form-actions">
    		<div class="col-sm-12">

            	<div class="row form-controls">
            		<div class="col-xs-5">
            			<select name="source" data-translate="source" class="form-control" data-toggle-select="target" required>
            				<option value="">Select your language</option>
                            <?php foreach ($lang->langs as $code => $name) : ?>
                            <option<?php if (isset($_SESSION[$session]['source']) && $_SESSION[$session]['source'] == $code) : ?> selected="selected"<?php endif; ?> value="<?= $code; ?>"><?= $name; ?></option>			
                            <?php endforeach; ?>
            			</select>
            		</div>
          		    <div class="col-xs-2">
            			<button data-action="toggle-select" type="button" class="btn btn-default"><span class="glyphicon glyphicon-refresh"></span></button>
            		</div>
            		<div class="col-xs-5">
            			<select name="target" data-translate="target" class="form-control" data-toggle-select="source">
            			    <option value="">Do not translate</option>
                            <?php foreach ($lang->langs as $code => $name) : ?>
                            <option<?php if (isset($_SESSION[$session]['target']) && $_SESSION[$session]['target'] == $code) : ?> selected="selected"<?php endif; ?> value="<?= $code; ?>"><?= $name; ?></option>			
                            <?php endforeach; ?>
            			</select>
            			<!-- 
            			<button type="button" data-action="translate">Translate</button>
            			-->
            		</div>
                </div>

                <div class="input-group form-actions">
                    <input type="text" name="text" data-translate="source" class="form-control" required>
                    <span class="input-group-btn">
                        <button type="submit" data-action="submit" class="btn btn-default"><span class="glyphicon glyphicon-send"></span></button>
                    </span>
                </div>
                        
    		</div>
    	</div>
    
    </form>
    
    <div id="audio"></div>

</div>

<script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

<script>

var form = $('#chat-form');
var output = $('.chat-output .list-group');
var source = form.find('input[data-translate="source"]');
var user = form.find('input[name=user]');
var guest = '<?= (isset($_SESSION[$session]['user'])) ? $_SESSION[$session]['user'] : $guest; ?>';
var session = <?= (isset($_SESSION[$session]['user'])) ? 'true' : 'false'; ?>;
var link = 'https://example.com/chat/';

var speech_enabled = false;

meSpeak.loadConfig("js/mespeak/mespeak_config.json");
meSpeak.loadVoice("js/mespeak/voices/en.json");

function loadLang(id, callback) {
    meSpeak.loadVoice("js/mespeak/voices/" + id + ".json", callback);
}

function prompt_name() {
	
	form.find('button[data-action=edit-user]').addClass('active');
	var name = (user.val()) ? form.find('input[name=user]').val() : guest;
    var _prompt = prompt("Enter a nickname", name);
    form.find('button[data-action=edit-user]').removeClass('active');
    if (_prompt.length) {
        form.find('input[name=user]').val(_prompt);
        form.find('button[data-action=edit-user]').text(_prompt);

    } else {
    	prompt_name();
    }
}

function prompt_share() {
	var _link = '<?= $session; ?>';
	form.find('button[data-action=share]').addClass('active');
    prompt("Copy this link and share it", link + '?s=' + _link);
    form.find('button[data-action=share]').removeClass('active');
}

function load_log() {

	var log_index = $('.chat-output li').length;
	
	$.ajax({
	    url: 'log.php?s=<?= $session; ?>&index=' + log_index,
	    success: function (data) {
		    if (data) {
			    
		    	 output.append(data);
		    	 
		    	 var rows = log_index - $('.chat-output li').length;
		    	 log_index = $('.chat-output li').length;

		    	 if (rows < 0) {

			    	 if (speech_enabled) {
			    		 var speak_text = []; 
			    		 $('.chat-output li:gt("' + (log_index + rows - 1) + '")').each(function () {
			    			 speak_text.push({lang: $(this).data('target'), text: $(this).find('em').text()});
					     });
			    		 speech(speak_text, 0);
				     }
				     window.scrollTo(0, document.body.scrollHeight);
			     }
		    }
    	}
	});	
}



function speech(rows, index, lang) {

	var speech_index = index;
	var speech_lang = lang;
	
	if (speech_index < rows.length) {
		if (speech_lang != rows[speech_index].lang) {
			meSpeak.loadVoice("js/mespeak/voices/" + rows[speech_index].lang + ".json", function () {
		    	speech_lang = rows[speech_index].lang;
		    	meSpeak.speak(rows[speech_index].text, {}, function () {
		    		speech_index++;
		    		speech(rows, speech_index, speech_lang);
		    	});
			});
	    	
		} else {
        	meSpeak.speak(rows[speech_index].text, {}, function () {
        		speech_index++;
        		speech(rows, speech_index, speech_lang);
        	});
		}
	}
	 
}

$(function () {

	if (!session) {
	    user.prop('disabled', false);
	    $('button[data-action=edit-user]').prop('disabled', false);
	}
		
	$('button[data-action="toggle-select"]').click(function () {
		$(this).toggleClass('active');
		
		var _source = $('select[data-toggle-select][name="source"]');
		var _target = $('select[data-toggle-select][name="target"]');

		var source_val = _source.val();
		var target_val = _target.val();
		
	    _source.val(target_val);
	    _target.val(source_val);
	});

	$('button[data-action="toggle-audio"]').click(function () {
	    $(this).toggleClass('active');
	    speech_enabled = ($(this).is('.active')) ? true : false;
	});

	$('button[data-action="edit-user"]').click(function () {
	    prompt_name();
	});

	$('button[data-action="share"]').click(function () {
	    prompt_share();
	});
	
    form.submit(function () {

    	var fields = form.serialize();

    	var _request = fields.split('+').join(" ").split('&');
    	var _fields = {};
    	
    	$.each(_request, function (index, value) {
	    	var _par = value.split('=');
	    	var _name = _par[0];
	    	var _value = _par[1];
	    	_fields[_name] = _value;
    	});
    	
    	$.ajax({
    	    url: form.attr('target'),
    	   	method: 'POST',
    	   	data: fields,
    	    success: function (result) {
    	    	//output.text(result);
    	    	user.prop('disabled', true);
    		    $('button[data-action=edit-user]').prop('disabled', true);
    	    	source.val('').attr('placeholder', _fields.text);
    	    	load_log();
    	    }
    	});
    	
    	//translate(source.text.val(), source.lang.val(), target.lang.val());	

    	return false;
    });

});

setInterval(function () {
	load_log();
}, 3000);

$(window).load(function () {
    <?php if (!isset($_SESSION[$session]['user'])) : ?>
    prompt_name();
    <?php endif; ?>
});

</script>

</body>

</html>
