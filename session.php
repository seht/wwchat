<?php

if (!isset($_GET['s'])) {
    exit();
}
if (!isset($_POST['source'])) {
    exit();
}
if (!isset($_POST['target'])) {
    exit();
}
if (!isset($_POST['text'])) {
    exit();
}

$session = $_GET['s'];

function sanitize_input(&$input) {
    return $input = filter_var(strip_tags(trim($input)), FILTER_SANITIZE_STRING);
}

session_start();

define('KEY', 'trnsl.1.1.20150227T115726Z.512686e18ec4bb96.bd2071b152cef988e4fb8640eeff234bade92146');

sanitize_input($_POST['user']);
sanitize_input($_POST['source']);
sanitize_input($_POST['target']);
sanitize_input($_POST['text']);

if (!$_POST['text']) {
    exit();
}

if (!isset($_SESSION[$session]['user'])) {
    $_SESSION[$session]['user'] = ($_POST['user']) ? $_POST['user'] : 'guest-' . time();
}
$_SESSION[$session]['source'] = $_POST['source'];
$_SESSION[$session]['target'] = $_POST['target'];

$lang = ($_POST['source'] && $_POST['target']) ? $_POST['source'] . '-' . $_POST['target'] : $_POST['target'];

$text = $_POST['text'];

if ($lang) {

    $args = [
        'key' => KEY,
        'lang' => $lang,
        'text' => sanitize_input($_POST['text'])
    ];
    
    $query = '?' . http_build_query($args);
    
    $result = json_decode(file_get_contents('https://translate.yandex.net/api/v1.5/tr.json/translate' . $query));
    
    $code = $result->code;
    
    if ($code > 200) {
        print $code;
        exit();
    }
    
    $text = trim(current($result->text));
    //$lang = explode('-', $result->lang);
    //$_SESSION[$session]['source'] = $lang[0];
    //$_SESSION[$session]['target'] = $lang[1];
}

if (!$_SESSION[$session]['target']) {
    $_SESSION[$session]['target'] = $_SESSION[$session]['source'];
}

$file = trim(file_get_contents($session . '.json'));
$log = ($file) ? json_decode($file) : [];
if (is_array($log)) {
    $log[] = ['user' => $_SESSION[$session]['user'], 'time' => date('H:i:s'), 'text' => $text, 'source' => $_SESSION[$session]['source'], 'target' => $_SESSION[$session]['target']];
    file_put_contents($session . '.json', json_encode($log));
}

exit();
